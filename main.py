import nltk
from nltk.corpus import stopwords
import os.path
import sys
import urllib2
import re
from bs4 import BeautifulSoup
import enchant

f = None
filename = 'applinks.txt'
count = 3
verbs = {}
d = enchant.Dict("en_US")
stop_words = set(stopwords.words('english'))

TEST_ONE_LINK = 1
LINK = 'https://itunes.apple.com/us/app/apple-store/id375380948?mt=8'


def removeFromResult(soup):
	global stop_words

	for e in soup.findAll('br'):
		e.extract()


def getDescription(response):
	html = response.read()
	soup = BeautifulSoup(html, "lxml")
	   
	removeFromResult(soup)
	result = soup.find("p", {"itemprop": "description"}) #.text

	if result == None:
		return None
	else:
		num_of_sentences = len(result.contents)
		return result.contents

def selectVerbsInSentence(link, tagged):
	global verbs
	next = 0
	for i in range(len(tagged)):
		if tagged[i][1] == 'VB' or tagged[i][1] == 'VBG' or tagged[i][1] == 'VBD' or tagged[i][1] == 'VBP' or tagged[i][1] == 'VBZ':
			verbs.setdefault(link,[])
			if tagged[i][0].lower() not in verbs[link]:
				verbs[link].append(tagged[i][0].lower())
				next = 1
		if next == 1 and (tagged[i][1] == 'NN' or tagged[i][1] == 'NNS'):
			verbs[link][-1] = verbs[link][-1] + ' ' + tagged[i][0]
			next = 0

def removeStopWords(tagged):
	for tuple in tagged:
		if tuple[0] in stop_words:
			tagged.remove(tuple)
	print tagged
	return tagged


def getVerbs(link, response):
	sentences = getDescription(response)
	if sentences == None:
		return
	print sentences

	for i in range(len(sentences)):
		#print d.check(sentences[i])

		#print sentences[i]
		tokens = nltk.word_tokenize(sentences[i])
		tagged = nltk.pos_tag(tokens)
		tagged = removeStopWords(tagged)
		selectVerbsInSentence(link, tagged)

def main():
	global count
	global verbs

	if TEST_ONE_LINK == 1:
			response = urllib2.urlopen(LINK)
			getVerbs(LINK, response)
			return

	if os.path.isfile(filename):
		f = open(filename, 'r')
	else:
		print "No such file. Exiting..."
		sys.exit()

	if f != None:
		for link in f:
			#print link
			response = urllib2.urlopen(link.strip())
			getVerbs(link, response)
			#print verbs
			count -= 1
			if count == 0: 
				break
	else:
		print "File pointer is None"

def printVerbs():
	for key in verbs:
		for i in range(len(verbs[key])):
			print verbs[key][i]


main()
printVerbs()
#print verbs



