import nltk
from nltk.corpus import stopwords
import os.path
import sys
import urllib2
import re
from bs4 import BeautifulSoup
import enchant
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer
import string
import unicodedata

f = None
filename = 'applinks.txt'
count = 3
verbs = {}
d = enchant.Dict("en_US")
stop_words = set(stopwords.words('english'))


def removeFromResult(soup):
	for e in soup.findAll('br'):
		e.extract()


def getDescription(response):
	html = response.read()
	soup = BeautifulSoup(html, "lxml")
	   
	removeFromResult(soup)
	result = soup.find("p", {"itemprop": "description"}) #.text

	if result == None:
		return None
	else:
		num_of_sentences = len(result.contents)
		return result.contents

def selectVerbsInSentence(link, tagged):
	global verbs
	next = 0
	for i in range(len(tagged)):

		if (tagged[i][0] == "Do" or tagged[i][0] == "Does") and next == 0:
			verbs.setdefault(link,[])
			if tagged[i][0] not in verbs[link]:
				verbs[link].append(tagged[i][0])
				next = 1

		elif tagged[i][1] == 'VB' or tagged[i][1] == 'VBG' or tagged[i][1] == 'VBD' or tagged[i][1] == 'VBP' or tagged[i][1] == 'VBZ':
			if next == 0:
				verbs.setdefault(link,[])
				next = 1
			if len(verbs[link]) > 0 and len(verbs[link][-1].split()) == 1:
				verbs[link].pop()
			if tagged[i][0] not in verbs[link]:
				verbs[link].append(tagged[i][0])
					
		elif next == 1:
			verbs[link][-1] = verbs[link][-1] + ' ' + tagged[i][0]
			if (len(tagged) > i+1) and (tagged[i][1] == 'NN' or tagged[i][1] == 'NNS'):
				if (tagged[i+1][1].strip() != "and"):
					next = 0

def removeStopWords(tagged):
	for tuple in tagged:
		if tuple[0] in stop_words:
			tagged.remove(tuple)
	#print tagged
	return tagged

def replaceWords(s):

	s = s.lower()
	s = s.replace("don't", "do not")
	s = s.replace("doesn't", "does not")
	s = s.replace("who'll", "who will")
	s = s.replace("it's", "")
	s = s.replace("is", "")
	s = s.replace("are", "")
	s = s.replace("he", "")
	s = s.replace("she", "")
	s = s.replace("am", "")
	printable = set("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\ -")
	s = filter(lambda x: x in printable, s)
	
	unicodedata.normalize('NFKD', s).encode('ascii', 'ignore')

	d = enchant.Dict("en_US")
	new_str = ""
	for w in s.split(" "):
		if len(w) > 1 and d.check(w) == True:
			new_str += " " + w
	return new_str

def getVerbs(link, response):
	sentences = getDescription(response)
	if sentences == None:
		return
	#print sentences

	for i in range(len(sentences)):
		print sentences[i]
		sentences[i] = replaceWords(sentences[i])
		tokens = nltk.word_tokenize(sentences[i])
		tokens = filter(lambda word: word not in '":?|\/*.,-', tokens)
		tagged = nltk.pos_tag(tokens)
		tagged = removeStopWords(tagged)
		print tagged
		selectVerbsInSentence(link, tagged)


def main():
	global count
	global verbs

	if len(sys.argv)  == 2:
			response = urllib2.urlopen(sys.argv[1])
			getVerbs(sys.argv[1], response)
			return

	if os.path.isfile(filename):
		f = open(filename, 'r')
	else:
		print "No such file. Exiting..."
		sys.exit()

	if f != None:
		for link in f:
			#print link
			response = urllib2.urlopen(link.strip())
			getVerbs(link, response)
			#print verbs
			count -= 1
			if count == 0: 
				break
	else:
		print "File pointer is None"

def printVerbs():
	for key in verbs:
		for i in range(len(verbs[key])):
			print verbs[key][i]


main()
printVerbs()




